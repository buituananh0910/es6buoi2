

export const renderGlassesList = (dataGlasses) => {
  let contentHTML = "";
  dataGlasses.forEach((glasses) => {
    contentHTML += `<a class="col-4">
        <img onclick="showThongTinLenForm('${glasses.id}')" src="${glasses.src}" alt="" class="img-fluid">
        </a>`;
  });
  document.getElementById("vglassesList").innerHTML = contentHTML;
};

export const renderInfoGlasses = (glasses) => {
  return `<div><h3>${glasses.name} - ${glasses.brand} (${glasses.color})</h3> 
    <span>${glasses.price}$ </span>
    <p>${glasses.description}</p></div>`;
};

export const renderGlasses = (glasses) => {
  return `<div>
    <img src="${glasses.virtualImg}" >
    </div>`;
};

export function timKiemViTri(id, listglass) {
  for (var i = 0; i < listglass.length; i++) {
    var glass = listglass[i];
    if (glass.id == id) {
      return i;
    }
  }
  return -1;
}